/*console.log("Hello World")*/
//create trainer

let trainer1 = {
	name: "Hanamichi Sakuragi",
	age: 16,
	pokemon: [
			"Pikachu", 
			"Charizard", 
			"Bulbasaur",
			"Squirtle"
			],
	friends: {
		hoen: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},

	talk: function(){
		console.log("Pikachu! I choose you!")
	},
}

console.log(trainer1);
console.log("Result of Dot notation:");
console.log(trainer1.name);
console.log("Result of square bracker notation:");
console.log(trainer1["pokemon"]);
console.log("Result of talked method");
trainer1.talk();


//Create a constructor function
function Pokemon (name, level){
	this.name = name;
	this.level = level;
	this.health = 1.5 * level;
	this.attack = level;






	//add tackle method

	this.tackle = function(target){

		console.log(this.name + " tackled " +target.name)
			console.log(target.name + "'s health now is reduced to " + Number(target.health - this.attack));
		let tackled = Number(target.health - this.attack);	
			
	if(tackled <= 0){
				target.faint();
			}
	
	}
	this.faint = function(){
		console.log(this.name + " fainted ")
	}
}



let pikachu = new Pokemon("Pikachu", 5);
let bulbasaur = new Pokemon("Bulbasaur", 4);
let charizard = new Pokemon("Charizard", 2);
console.log(pikachu)
console.log(bulbasaur)
console.log(charizard)



bulbasaur.tackle(pikachu);
charizard.tackle(bulbasaur);
pikachu.tackle(charizard);










