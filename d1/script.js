/*console.log("Hello World")*/

//array
/*let grades = [98.5, 94.3, 89.2, 90.1];
console.log(grades[0]);*/

//An object is similar to an array.
//A collection of related data or functionality.
//usually it represents real world object
/*array vs object
Syntax:
array =[]
object = {}
*/
//object key-value pair.
//object initializer/literal notation - objects consists of properties, which are used to described an object. Key-value pair.
let = grade = {
	math: 89.2,
	english: 98.5,
	science: 90.1,
	filipino: 94.3
}

//We used dot notation to access the properties/values of our object
console.log(grade.english);
//Syntax: literal notation
/*
	let objecName = {
	keyA: Valuea,
	keyB: ValueB
	}
*/

let cellphone = {
	brandName: "Nokia 3310",
	color: "Dark Blue",
	manufactureDate: 1999
}
console.log(typeof cellphone);
//can pu object inside object
let student = {
	firstName: "John",
	lastName: "Smith",
	mobileNo: 0912345612344,
	location: {
		city: "Tokyo",
		country: "Japan"
	},

	emails: ["john@gmail.com", "johnsmith@mail.com"],

	//object method - function inside an object
	fullName: function(){
		return this.firstName + " " + this.lastName
	}
}
console.log(student.fullName())
console.log(student.location.city)
//dot notation only in object to avoid confusion
//bracket notation
/*console.log(student["firstName"])*/
console.log(student.emails[0]);
console.log(student.emails);
//we can also put function in the objects
//this notation = only refers to the owner of the function



//sample for this keyword
//no need parameter as of now
const person1 = {
	name: "Jane",
	greeting: function(){
		return "Hi I\'m " + this.name 
	}
}
console.log(person1.greeting());


//array of objects
let contactList = [
	{
		firstName: "John",
		lastName: "Smith",
		location: "Japan"
	},

	{
		firstName: "Jane",
		lastName: "Smith",
		location: "Japan"
	},

{
		firstName: "Jasmine",
		lastName: "Smith",
		location: "Japan"
	}


]
console.log(contactList[0].firstName)

//example

let people = [

{
	name: "Juanita",
	age: 13
},
{
	name: "Juanito",
	age: 14
},
{
	name: "Juan",
	age: 15
}
]
//printing all names in the console
people.forEach(
function(person){
	console.log(person.name)	
})

console.log(`${people[0].name} are the list`)
/*
//constructor - reusable function
//Creating object using a Constructor function(JS Object Constructor/Object from Blueprints)
*/
/*Creates a reusable function to create several objects that have the same data structure*/
/*This is useful for creating multiple copies/instances of an object*/
//Object Literals
//let object = {}
//Instance
//let object = new object
//An instance is a concrete moccurence of any object which emphasize on the unique identity of it
/*
Syntax:
	function objectName(KeyA, KeyB){
	this.keyA = KeyA,
	this.keyB = KeyB
	}
*/

//pwedeng gamitin ng paulit ulit
function Laptop(name, manufactureDate){
	//The this keyword allows to assign a new object property by associating them with the values received from our parameter
	this.name = name,
	this.manufactureDate = manufactureDate
}

//This is a unique instance of the Laptop object
//need the word new to create new
let laptop = new Laptop("Notebook", 2008);

console.log(laptop);

//This is another unique instance of the laptop object

let myLaptop = new Laptop("Macbbok Air", 2020)

console.log(myLaptop);

let oldLaptop = new Laptop("Portal", 2010)
console.log(oldLaptop);

//Creating empty objects
let computer = {}; //object literals
let myComputer = new Object(); //instance

console.log(myLaptop.name)

let array = [laptop, myLaptop]
console.log(array[0].name)

//initializing/Adding/Deleting/Reassigning Object Properties
//This is useful for times when an object's properties are undetermined at the time of creating them
oldLaptop.name = "ball"
console.log(oldLaptop.name)



let car = {}

//adding an object properties, we can use dot notation
car.name = "Honda Civic"
console.log(car)

car.manufactureDate = 2019;
console.log(car);
//reassigning the value with the same property name:
car.name = "Volvo"
console.log(car);

//deleting object properties
//just put delete
delete car.manufactureDate;
console.log(car);

//object method - called in the function inside an object

//OBJECT METHODS
//A method is a function which is a propertry of an object
//They are also functions and one of the key difference they have is that methods are functions related to a specific object.

//example
let person = {
	name: "John",
	talk: function(){
		console.log("Hello my name is " + this.name)
	}
}

console.log(person)
person.talk();


//Adding method to object person

person.walk = function(){
	console.log(this.name + " walked 25 steps forward")
}
person.walk()

//Method can use for reusable function
let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address: {
		city: "Austin",
		country: "Texas"
	},
emails: ["joe@mail.com", "joesmith@mail.com"],
introduce: function(){
	console.log("Hello my name is " + this.firstName + " " + this.lastName)
	}
}

friend.introduce();

//Example we will create pokemon game
/*
Real World Application of Objects
-scenario
	1. We would like to create a game that would have several pokemon interact with each other
	2. Every pokemon would have the same set of stats, properties and functions
*/
//Using object literals to create multilpe kinds of pokemon would be time consuming
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled target Pokemon")
		console.log("targetPokemon health is now reduced to _targetPokemonHealth_")
	},
	faint: function(){
		console.log("Pokemon fainted")
	}
}

//Creating an object constructor instead of object literals.

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;


	//methods

	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name)
		console.log("targetPokemon's health now reduced in _targetPokemonHealth_")
	};

	this.faint = function(){
		console.log(this.name + " fainted ")
	}
}

//create instances for example

let pikachu = new Pokemon("Pikachu", 16);
let charizard = new Pokemon("Charizard", 8);
/*console.log(pikachu)*/

pikachu.tackle(charizard);

/*Mini activity*/













































































































































